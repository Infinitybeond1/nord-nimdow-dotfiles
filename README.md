# nord-nimdow-dotfiles

Dependencies: <br>
Thunar - File Manager `sudo pacman -S thunar`<br>
dunst - Notification Utility `sudo pacman -S dunst`<br>
nimdow - Tiling Window Manager, can be installed from the aur `yay -S nimdow-bin`<br>
ranger - Terminal File Manager `sudo pacman -S ranger`<br>
neofetch - UNIX fetch utility `sudo pacman -S neofetch`<br>
kitty - Terminal emulator (needed for neofetch images) `sudo pacman -S kitty`<br>
ion - shell written in rust `yay -S ion-git`<br>
<br>
More documentation coming soon...
